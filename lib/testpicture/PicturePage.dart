/*
* Copyright (c) 2023 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PicBean {
  String name;
  int id;
  PicBean(this.name, this.id);
}

List<PicBean> pics = [
  PicBean("1.png", 0),
  PicBean("2.jpeg", 0),
  PicBean("3.jpeg", 0),
  PicBean("4.jpg", 0),
  PicBean("6.jpg", 0),
];

class PicturePage extends StatefulWidget {
  const PicturePage({super.key});
  @override
  State<PicturePage> createState() => _PicturePageState();
}
class _PicturePageState extends State<PicturePage> {
  final MethodChannel _channel = const MethodChannel('PictureChannel');

  @override
  void initState() {
    super.initState();
    registerTextures();
  }
  @override
  void dispose() {
    super.dispose();
    unregisterTextures();
  }
  void registerTextures() async {
    for (PicBean pic in pics) {
      var id =
      await _channel.invokeMethod("registerTexture", {'pic': pic.name});
      setState(() {
        pic.id = id;
      });
    }
  }

  void unregisterTextures() async {
    for (PicBean pic in pics) {
      await _channel.invokeMethod('unregisterTexture', {'textureId': pic.id});
    }
  }

  Widget getTextureBody(BuildContext context, PicBean picBean) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: 300,
          height: 300,
          child: Texture(textureId: picBean.id),
        ),
        Container(
          height: 10,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("daex_texture"),
      ),
      body: ListView.builder(
        itemCount: pics.length,
        itemBuilder: (context, index) {
          PicBean picBean = pics[index];
          return picBean.id >= 0
              ? getTextureBody(context, picBean)
              : const Text('loading...');
        },
      ),
    );
  }
}
