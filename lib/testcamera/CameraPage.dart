/*
* Copyright (c) 2023 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;

class CameraPage extends StatefulWidget {
  const CameraPage({super.key});
  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  final MethodChannel _channel = MethodChannel('CameraControlChannel');

  int textureId = -1;

  @override
  void initState() {
    super.initState();

    newTexture();
    startCamera();
  }

  @override
  void dispose() {
    super.dispose();
    if (textureId >= 0){
      _channel.invokeMethod('unregisterTexture', {'textureId': textureId});
    }
  }

  void startCamera() async {
    await _channel.invokeMethod('startCamera');
  }

  void newTexture() async {
    int id = await _channel.invokeMethod('registerTexture');
    setState(() {
      this.textureId = id;
    });
  }

  Widget getTextureBody(BuildContext context) {
    return Container(
      width: 500,
      height: 500,
      child: Texture(
        textureId: textureId,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget body = textureId>=0 ? getTextureBody(context) : Text('loading...');
    print('build textureId :$textureId');

    return Scaffold(
      appBar: AppBar(
        title: Text("daex_texture"),
      ),
      body: AspectRatio(
        aspectRatio: 1,
        child:Transform.rotate(
          angle: 90 * math.pi / 180,
          child: Container(
            color: Colors.white,
            height: 500,
            width: 500,
            child: Center(
              child: body,
            ),
          ),
        ),
      ),
    );
  }
}
