import 'package:flutter/material.dart';
// 导入各个应用的主页面
import 'camera/main.dart' as camera;
import 'platform_view/main.dart' as platform_view;
import 'testcamera/main.dart' as testcamera;
import 'testpicture/main.dart' as testpicture;
import 'video_player/main.dart' as video_player;
import 'webview/main.dart' as webview;

void main() async {
  // Fetch the available cameras before initializing the app.
  await camera.initializeCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '多应用集成',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _num = 0;

  void _incrementNum() {
    setState(() {
      _num++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('集成测试应用'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('局部变量值: $_num', style: TextStyle(fontSize: 18)),
                ElevatedButton(
                  onPressed: _incrementNum,
                  child: Text('增加'),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                _buildAppButton(context, 'Camera', () => camera.MyApp()),
                _buildAppButton(context, 'Platform View', () => platform_view.MyApp()),
                _buildAppButton(context, 'Test Camera', () => testcamera.MyApp()),
                _buildAppButton(context, 'Test Picture', () => testpicture.MyApp()),
                _buildAppButton(context, 'Video Player', () => video_player.MyApp()),
                _buildAppButton(context, 'WebView', () => webview.MyApp()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAppButton(BuildContext context, String title, Widget Function() appBuilder) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        child: Text(title),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => appBuilder()),
          );
        },
      ),
    );
  }
}